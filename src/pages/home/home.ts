import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import { DetalleProductoPage } from '../detalle-producto/detalle-producto';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	productos: Array<any>
 /*= [
    {
        "description": "Limpia mejor",
        "id": 1,
        "image": "http://lorempixel.com/400/200/nightlife",
        "name": "Cepillo de Dientes1",
        "price": "1.85",
        "shops": [
            1
        ]
    },
    {
        "description": "Helado sabroso",
        "id": 2,
        "image": "http://lorempixel.com/400/200/nightlife",
        "name": "Magnum de avellana",
        "price": "5500.00",
        "shops": [
            1
        ]
    }
]*/;

  constructor(
  	public navCtrl: NavController, 
    public http: Http,
  	public modalCtrl: ModalController) {

  }

  ionViewDidLoad(){
    //Obtiene los datos del servidor
  	this.getRemoteData();
    console.log(this.productos);
  }

  getRemoteData(){
    // Llama al servidor y obtiene el json de todos los producto
    this.http.get('http://krono-training-env.42rrceeisp.us-east-1.elasticbeanstalk.com/krato_articles/tienda/1/products/').subscribe(data => {             
      console.log(data.json());
      //Almacena el json en la variable local productos
      this.productos = data.json();
    });
  }

  //Muestra el modal del prodcuto
  goToDetalles(product){
  	let modal = this.modalCtrl.create(DetalleProductoPage, {
  		product: product
  	});
  	modal.present();
  }
}


